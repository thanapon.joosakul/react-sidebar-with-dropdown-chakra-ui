import React from 'react'
import { Box, Stack } from '@chakra-ui/react'
import Topbar from '../common/Topbar';
import Sidebar from '../common/Sidebar';
import { Outlet } from 'react-router-dom';
import colorConfigs from '../../configs/colorConfigs';
import sizeConfigs from '../../configs/sizeConfigs';

type Props = {};

const MainLayout = (props: Props) => {
  return (
    <Box display='flex'>
      <Topbar />
      <Box 
        as='nav'
        w={sizeConfigs.sidebar.width}
        flexShrink={0}
      >
        <Sidebar />
      </Box>
      <Box 
        as='main'
      >
        <Outlet />
      </Box>
    </Box>
  )
}

export default MainLayout;