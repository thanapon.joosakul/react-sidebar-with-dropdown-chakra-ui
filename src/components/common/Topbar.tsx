import React from 'react'
import { 
  Box,
  Container,
  Heading,
  Text
} from '@chakra-ui/react'
import colorConfigs from '../../configs/colorConfigs';
import sizeConfigs from '../../configs/sizeConfigs';

type Props = {};

const Topbar = (props: Props) => {
  return (
    <Box
      ml={sizeConfigs.sidebar.width}
      w={`calc(100% - ${sizeConfigs.sidebar.width})`}
      position='fixed'
      alignItems={'center'}
      display={'flex'}
      pl='24px'
      pr='24px'
      minH='64px'
    >
      <Text fontSize='32px' fontWeight='bold'>React Sidebar with dropdown</Text>
    </Box>
  )
}

export default Topbar;